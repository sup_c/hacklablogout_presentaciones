# Encriptacio i navegació anonima #

#VSLIDE

#### Índex

1. Navegació anònima
	- TorBrowser / tails
	- VPN
2. Metadades
	- Què són les metadades
	- Com esborrar les metadades
3. Geolocalització en dispositius mòbils
4. Encriptació
	- Què és l'encriptació i com funciona?
	- Guia bàsica de com encriptar dispositius android
	- Encriptació en l'ordinador

#VSLIDE

## Hola!
* Autoria: 2020 Hacklab Logout
* [Llicència: CC - Reconeixement i Compartir igual](https://creativecommons.org/licenses/by-sa/4.0/deed.ca)

#VSLIDE
## Roda de motivacions

«Què esperes d'aquesta sessió?»

«Per què estàs aquí?»

#VSLIDE
## Sondeig tècnic

* Mail: gmail - riseup
* OS: windows - ubuntu
* Anon: Tor, metadades, cares
* Mòbil: precaucions?

#VSLIDE
## Seguretat
* Relativa al context
* De grup
* Psico-social + física + info
* Progressiva
* Objectiu: activisme sostenible


#HSLIDE

# Navegació anonima #

#VSLIDE
### Anonimat a la xarxa: Tor
![Logo de Tor](tor/img/tor-logo.png)

#VSLIDE

#### Tor és
* Una **xarxa** d'ordinadors sobre internet per anonimitzar.
* El **programa** que connecta els ordinadors a la xarxa Tor.
* El **navegador** que funciona a través de la xarxa Tor.

#VSLIDE

#### Funcions
* Amaga l'adreça IP
* Xifra el trànsit
* Serveis onion

#VSLIDE

#### Contrapartides
* Retard i ample de banda limitat
* Crida l'atenció
* Bloquejos, traves

#VSLIDE

#### Adreça IP

**Destí** i **remitent** d'una comunicació a internet.

El nostre remitent és una **adreça pública**.

Pot ser la de:
* casa
* biblioteca
* local social
* …

#VSLIDE

#### Adreça IP

Les ISP tenen comprats **rangs** d'adreces IP, i estan repartits per **zones** geogràfiques. Per tant, la nostra adreça IP pública revela:
* La companyia telefònica contractada
* La localització física aproximada

#VSLIDE

#### Adreça IP
Podem esbrinar la informació que escampem per internet amb serveis com [ipleak.net](https://ipleak.net)
![Adreça IP](tor/img/ip-leak_clearnet.gif)

#VSLIDE

#### Internet ≠ WWW
A Internet hi funcionen molts protocols d'aplicació. Per conviure en la mateixa màquina, es fan servir ports TCP o UDP.

La Web fa servir els següents ports
* HTTP: TCP-80
* HTTPS: TCP-443
* Resolució de noms (DNS): UDP-53

#VSLIDE

#### Internet ≠ WWW
Però n'hi ha molts més, de protocols
* Xat (Jabber): TCP-5222
* Correu: TCP-993, TCP-587, TCP-465
* Age of Empires II: TCP-47624

#VSLIDE

#### Internet ≠ WWW
Actualment la tendència és definir nous protocols per sobre de HTTP: les **API web**
* API de Twitter
* Xat (Matrix.org, RocketChat)
* Xarxes socials lliures (federació entre servidors)

#VSLIDE

#### Connexió HTTP, sense xifrar
![Connexió HTTP](tor/img/internet_16-9.png)

#VSLIDE

#### Connexió xifrada, TLS.
Xifra la connexió entre client i servidor.
* Web, mail, xat, …

Els túnels TLS ens protegeixen d'atacs:
* **Passius**: robar credencials, llegir formularis i missatges
* **Actius**: manipular webs, programes, actualitzacions.

#VSLIDE

#### Connexió HTTPS, xifrada

![Connexió HTTPS](tor/img/tls-internet_16-9.png)

#VSLIDE

#### Adreça IP del remitent **sense** usar **Tor**
Des de Barcelona i amb Movistar:
![Adreça IP sense Tor](tor/img/ip-leak_clearnet.gif)

#VSLIDE

#### Adreça IP del remitent **usant Tor**
Des de Barcelona i amb Movistar:
![Adreça IP amb Tor](tor/img/ip-leak_tor.gif)

#VSLIDE

#### Connexió a través de Tor
![Connexió amb Tor](tor/img/tor-internet_16-9.png)

#VSLIDE

#### Nodes de sortida a Clearnet

* Pocs: ample de banda limitat.
* Perillosos: Imprescindible HTTPS.

#VSLIDE

#### Connexió a través de Tor
![Connexió amb Tor i TLS](tor/img/tor-tls-internet_16-9.png)

#VSLIDE

#### Serveis onion

Comunicació entre desconeguts
* Servidor anònim.
* Usuària anònima.

En comparació a serveis a la clearnet:
* No congestiona els nodes de sortida.
* No necessita HTTPS.

#VSLIDE

#### Alguns llocs web onion

* Hidden Wiki: http://zqktlwi4fecvo6ri.onion/wiki/
* Críptica: http://cripticavraowaqb.onion
* Wikileaks: http://wlupld3ptjvsgwqw.onion/
* Webmail: http://sigaintevyh2rzvw.onion/

#VSLIDE

#### Aplicacions basades en serveis onion
* OnionShare: compartir fitxers
* Ricochet: xat auster

#VSLIDE

#### Aplicacions torificables:
* Navegador: Firefox → Tor Browser Bundle
* Correu: Thunderbird → extensió TorBirdy
* App Twitter oficial → app Twidere + Orbot

#VSLIDE

#### VPN

- Es molt semblant a tor, ens ajuda a ocultar la ip i navegar anonimament
- Es pot fer servir com una capa adicional de protecció conjuntament amb tor o per separat

#VSLIDE

#### Proveidors VPN fiables

- [RiseupVPN](https://riseup.net/ca/vpn)
- [NJALLA](https://njal.la/)

#HSLIDE

# Metadades #

#VSLIDE

### Què són les metadades #
- Són dades que donen informació sobre altres dades
- Exemple de metadades en un missatge:
  - Hora
  - Ubicació
  - Dispositiu

#VSLIDE

#### Veiem un example
Fotografia adorable...

![Nens menjant gelat](Metadata/nens_gelat.jpg)

#VSLIDE
#### Metadades EXIF
... amb més informació del que sembla!
```
user@host:~$ exiftool nens_gelat.jpg 
File Type         : JPEG
MIME Type         : image/jpeg
Camera Model Name : SM-G900V
ISO               : 64
Exif Version      : 0220
Flash             : No Flash
Aperture          : 2.2
GPS Altitude      : 18 m Below Sea Level
GPS Date/Time     : 2016:04:17 20:25:10Z
GPS Latitude      : 40 deg 42' 56.07" N
GPS Longitude     : 73 deg 50' 36.35" W
GPS Position      : 40 deg 42' 56.07" N, 73 deg 50' 36.35" W
Shutter Speed     : 1/120
Create Date       : 2016:04:17 16:25:24.890
Focal Length      : 4.8 mm (35 mm equivalent: 31.0 mm)
```

#VSLIDE
#### Metadades EXIF: model de càmera
```
Camera Model Name : SM-G900V
```
![EXIF: Model de la càmera](Metadata/exif_camera-model.png)

#VSLIDE
#### Metadades EXIF: ubicació
```
GPS Position      : 40 deg 42' 56.07" N, 73 deg 50' 36.35" W
```
![EXIF: Ubicació](Metadata/exif_gps.png)

#VSLIDE

# Com eliminar les metadades

#VSLIDE

## Esborrar metadades des d'un ordinador ##

- [MAT2](https://0xacab.org/jvoisin/mat2)([Web](https://matweb.info/), [Tails](https://tails.boum.org/doc/sensitive_documents/metadata/index.en.html), altres Linux per consola)
- [jExiftoolGui](https://github.com/hvdwolf/jExifToolGUI/releases) (Linux/Windows/Mac)

#VSLIDE

![Captura de la web del MAT2](Metadata/mat2-web.png)

#VSLIDE

![jExif](Metadata/jExifTool.png)

#VSLIDE
## Android ##

- ScrabledExif (F-Droid & Playstore)
- Es recomanable instalarla desde F-droid
- Signal porta una funcio incorporada, també deixa censurar cares.

#VSLIDE

![jExif](Metadata/Scrambled.jpg)

#VSLIDE
#### Resultat al borrar metadades

Després de borrar les metadades,
veiem que tota la informació compromesa ha estat esborrada.
```
user@host:~$ exiftool nens_gelat_neta.jpg 
File Type         : JPEG
MIME Type         : image/jpeg
```

#HSLIDE

# Geolocalització en dispositius mòbils #

#VSLIDE

## Xarxa de telefonia

* Xarxa "ceŀlular": composta per ceŀles.
* Estacions base amb 1 o més antenes
* Cada antena genera una "ceŀla de cobertura"
* Estacions base interconnectades

#VSLIDE

![mapa-estacions-base](img/telefon/mapa-estacions-base.png)

_Estacions base de Movistar al centre de Barcelona_

#VSLIDE

![estacio-base](img/telefon/estacio-base.png)

_Estació base de tres ceŀles_

#VSLIDE

## Retenció de dades

_«Espionatge massiu retroactiu»_

Les operadores de **telefonia** i proveïdores d'**internet** han d'emmagatzemar durant **1 any** totes la **informació associada** a les comunicacions transportin.

#VSLIDE

## Metadades retingudes

* Identificador del mòbil (IMEI)
* Identificador de la targeta (IMSI)
* Remitent i destí de trucades, sms
* Connexions de dades: volum, duració
* Hora i lloc (gràcies a les ceŀles)

#VSLIDE

## Conseqüències

* 1 targeta → 1 núm mòbil → 1 persona legal
* Rutina diària: llocs on trobar-te
* Graf de relacions socials: família, amistats…
* Presència demostrable en un lloc i hora

#VSLIDE

## Casos de publicació voluntària

* Spitz i Ockenden van reclamar-ne una còpia
* Glättli va emmagatzemar-les per compte propi

_Austràlia va aprovar lleis molt similars a les europees_

#VSLIDE

![retencio-dades-spitz](img/telefon/retencio-dades-spitz.png)

2009: [Malte Spitz, eurodiputat verd](http://www.zeit.de/datenschutz/malte-spitz-data-retention)

#VSLIDE

![retencio-dades-glaettli](img/telefon/retencio-dades-glaettli.png)

2013: [Balthasar Glättli, diputat suís verd](https://www.digitale-gesellschaft.ch/dr.html)

#VSLIDE

![retencio-dades-ockenden](img/telefon/retencio-dades-ockenden.png)

2015: [Will Ockenden, periodista australià](http://www.abc.net.au/news/2015-08-16/metadata-retention-privacy-phone-will-ockenden/6694152)

#VSLIDE

## Localització assistida (aGPS)

S'agilitza el GPS amb una aproximació a partir de

* Ceŀles telefòniques properes
* Les Wi-Fi properes (MAC dels AP)

#VSLIDE

## Perill

* Bases de dades mundials privades de Google
* Registre de Google de la teva posició

#VSLIDE

## Historial d'ubicacions de Google

> prediccions per als desplaçaments diaris, resultats de la cerca millorats i anuncis més útils tant a Google com en altres llocs.

#VSLIDE

> les dades d'ubicació es poden desar des de qualsevol dels dispositius en què hagis iniciat la sessió i per als quals hagis activat l'Historial d'Ubicacions.

Consulta el teu [historial d'ubicacions](https://www.google.com/maps/timeline "Google Maps: Cronologia")

#VSLIDE

![google-maps-cronologia](img/smart/google-maps-cronologia.jpg)

_L'aplicació de «Cronologia» iŀlustra el rastreig de Google_

#VSLIDE

## Desactiva la recoŀlecció d'ubicacions

* Respon sempre NO si et demanen permís per la ubicació
* [Android genèric](https://support.google.com/accounts/answer/3118687 "Activar o desactivar l'Historial d'Ubicacions")
* [Android 4.1-4.3](https://support.google.com/nexus/answer/2819558 "Accedir a la ubicació amb versions d'Android entre la 4.1 i la 4.3")

#HSLIDE

# Encriptació #

#VSLIDE

## Que es la encriptació/xifrat ##

Es el process de convertir una informació en un codi que idealment nomes pot ser interpretat per qui te la clau. Es fa per tal  d'evitar acces no autoritzat a informació xifrada.

#VSLIDE

#### Exemple mes basic de xifrat, el xifrat del cesar ####
- ningu podra desxifrar aquest codi -> qlqjx srgud ghvaliudu dtxhvw frgl 
![cesar](Xifrat/cesar.png) <!-- .element: style="width: 60%;margin: 0 0;" -->


#VSLIDE

### Llocs on es pot aplicar el xifrat ###

- Durant el transport (Xifrat punt a punt, xifrat d'enllaç)
- Xifrat en l'emmagatzamamnet/de dispositius

#VSLIDE
#### Xifrat punt a punt

![endtoend_transport_diagram](Xifrat/end-to-end-encryption-comparison.jpg)

#VSLIDE
#### Xifrat de disc dur

![disk_encryption_diagram](Xifrat/diskencryption.png)

#HSLIDE

# Com encriptar un dispositiu android #

#VSLIDE

# Passos previs #
- Carregar el mòbil al màxim
- **!! Deixar el mòbil endollat durant tot el procés !!**
- Fer còpia de seguretat de totes les dades
- Canviar el bloqueig a tipus password
- **Recorda** Aquests passos són susceptibles de canviar, segons la versió d'Android

#VSLIDE

<div class="left">

En primer lloc anem a _Ajustes_

</div>

<div class="right">

![step1](EncryptAnd/Step1.jpg) <!-- .element: style="width: 70%;margin: 0 0;" -->

</div>

#VSLIDE

<div class="left">

Dins de setting, anem a a la secció de seguretat.

</div>

<div class="right">

![step2](EncryptAnd/Step2.jpg) <!-- .element: style="width: 70%;margin: 0 0;" -->

</div>

#VSLIDE

<div class="left">

Dins de seguretat busquem una secció que s'anomeni xifrat o encriptació.
Cliquem en Encriptar telèfon

</div>

<div class="right">

![step3](EncryptAnd/Step3.jpg) <!-- .element: style="width: 70%;margin: 0 0;" -->

</div>
#VSLIDE

<div class="left">

Donem al botó de Encriptar dispositiu.
Ens demanarà que confirmem la contrasenya, un cop introduïda començarà el procés d'encriptació.

</div>

<div class="right">

![step4](EncryptAnd/Step4.jpg) <!-- .element: style="width: 70%;margin: 0 0;" -->

</div>

#VSLIDE

# Dispositiu encriptat #

#HSLIDE
# Com encriptar un dispositiu android (Xiaomi) #

#VSLIDE

# Passos previs #
- Carregar el mòbil al màxim
- !! Deixar el mòbil endollat durant tot el procés !!
- Fer còpia de seguretat de totes les dades
- Canviar el bloqueig a tipus password

#VSLIDE

<div class="left">

En primer lloc anem a _Ajustes_

</div>

<div class="right">

![xstep0](EncryptXiaomi/Step0.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>

#VSLIDE
<div class="left">

Després anem a la secció de sobre el telèfon, per tal d'activar les _opcions de desenvolupador_.

</div>

<div class="right">

![xstep1](EncryptXiaomi/Step1.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>

#VSLIDE
<div class="left">

Aquí premem sobre _Versión MIUI_, fins que aparegui un cartell confirmant-nos que les opcions de desenvolupador han estat activades

</div>

<div class="right">

![xstep2](EncryptXiaomi/Step2.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>


#VSLIDE

<div class="left">

Un cop activades les opcions de desenvolupador, tornem a la secció general de configuració. Aquí anem a _Ajustes adicionales_

</div>

<div class="right">

![xstep3](EncryptXiaomi/Step3.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>
#VSLIDE
<div class="left">

En _Ajustes adicionales_ anem a _opcions de desenvolupador_.

</div>

<div class="right">

![xstep4](EncryptXiaomi/Step4.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>

#VSLIDE
<div class="left">

- Dins de _opcions de desenvolupador_ fem scroll fins abaix del tot.
- Seleccionem l'última opció, _Cifra tu dispositivo usando la contraseña de bloqueo_

</div>


<div class="right">

![xstep5](EncryptXiaomi/Step5.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>

#VSLIDE

<div class="left">

- En aquesta secció activem l'únic boto que hi ha.
- Ens demanarà que confirmem el password del telefon, un cop fet començarà l'encriptació del dispositiu

</div>

<div class="right">

![xstep6](EncryptXiaomi/Step6.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>



#VSLIDE

# Dispositiu encriptat #

#HSLIDE

# Com encriptar un pendrive (amb LUKS) #

#VSLIDE

Instalar _cryptsetup_, si encara no esta instalat. Una posible manera és amb la següent comanda:

```
user@host:~$ sudo apt install cryptsetup
```

#VSLIDE

Trobar el pendrive que es vol encriptar amb la comanda _lsblk_.

![](./EncryptPen/lsblk.png)

A l'esquerra del tot, es distingeixen dos unitats, _sda_ i _sdb_ en aquest cas en particular.

El pendrive que s'encriptara és de 16GB. Mirant a la columna anomenada SIZE es veu que _sda_ és de 477GB i que _sdb_ de 14.5GB, per lo que el nostre pendrive és _sdb_ en aquest cas.



#VSLIDE

ara, suposant que el vostre pendrive és _sdb_

```
umount /dev/sdb1 
sudo cryptsetup luksFormat /dev/sdb
```

per encriptar el dispositiu. Si el vostre volum sigues sdc o sdd o qualsevol altre, s'han de escriure les mateixas comandes pero canviant _sdb_ pel que pertoqui.

**Atencio! Tota la informació del pendrive es perdera.**



#VSLIDE

En aquest pas s'ha d'especificar la contrasenya per desencriptar el dispositiu

![](./EncryptPen/luksFormat.png)



#VSLIDE

Falten només dos pasos!

```
sudo cryptsetup luksOpen /dev/sdb <nom_qualsevol>
```

on <nom_qualsevol> pot ser qualsevol seqüència de caracters que volgeu.

#VSLIDE

Per a crear una partició on guardar arxius (encriptats)

```
mkfs -t ext4 -L "<el_teu_nom>" /dev/mapper/<nom_qualsevol>
```

on <nom_qualsevol> ha de ser el mateix que hem ficat en el pas anterior

i <el_teu_nom> és el nom que tindrà el pendrive. Poden ser el mateix nom.

#VSLIDE

#### Pendrive encriptat!

Simplement treieu-lo de l'ordinador i torneu-lo a ficar i us demanara la contrasenya per a desencriptar.

#HSLIDE

# Com encriptar un pendrive (amb Veracrypt) #

#VSLIDE

### Instal·lar Veracrypt

Link: https://www.veracrypt.fr/en/Downloads.html

#VSLIDE

Des de VeraCrypt, clicar "Create Volume"

![](./EncryptPen/create_volume.png)

#VSLIDE

Clickar a la segona opció i "Next"

![](./EncryptPen/pas1.png)

#VSLIDE

Qualsevol de les dues opcions es correcta. La que preferiu

![](./EncryptPen/pas2.png)

#VSLIDE

Clickar a "Select Device..."

![](./EncryptPen/pas3.png)

#VSLIDE

Asegureu-vos de triar el vostre pendrive i no cap altre cosa!

Podeu esborrar arxius que no voleu esborrar si no aneu am compte aqui

![](./EncryptPen/pas4.png)

#VSLIDE

Simplement "Next"

![](./EncryptPen/pas5.png)

#VSLIDE

Revisar que heu fet una copia de seguretat d'allò que tenieu abans el pendrive, i "Yes".

![](./EncryptPen/pas6.png)

#VSLIDE

Pot ser demana la contrasenya d'administrador de l'ordinador, la que feu servir per entrar a la vostra sesió. Ficar la contra, "OK" i "Next".

![](./EncryptPen/pas7.png)

#VSLIDE

"Next".

![](./EncryptPen/pas8.png)

#VSLIDE

Posar la contrasenya que voelu fer servir per desencriptar el volum.

Que sigui llarga!

![](./EncryptPen/pas9.png)

#VSLIDE

Segurament us interesa la primera opció. Si no ho teniu clar, clickeu la segona. Després "Next".

![](./EncryptPen/pas10.png)

#VSLIDE

"Next"

![](./EncryptPen/pas11.png)

#VSLIDE

Mogeu el ratoli fins que us canseu, i després clickeu a "Format".

![](./EncryptPen/pas12.png)

#VSLIDE

Esteu apunt d'esborrar tot lo que tenieu al pendrive. Si esteu segures, "Yes".

![](./EncryptPen/pas13.png)

#VSLIDE

#### Volum encriptat!

"Exit" i podeu treure el pendrive quan volgeu.

![](./EncryptPen/pas14.png)

#VSLIDE

Cada cop que volgeu desencriptar el pendrive, torneu-lo a ficar. Inicieu VeraCrypt i clickeu "Auto-Mount Devices".

![](./EncryptPen/auto_mount.png)

#VSLIDE

Fiqueu la contrasenya que heu creat anteriorment, i "OK".

Es posible que torni a demanar la contrasenya d'administrador del vostre ordenador.

Pot trigar un minut en desencriptar el pendrive. Quan acabi, s'obrira automaticament la carpeta on podeu guardar arxius encriptats.

![](./EncryptPen/contra.png)

#HSLIDE

## Altres dispositius/dades que podem encriptar ##

- Ordinador
  - Linux
  - Mac
  - Windows
- Correu electronic

#VSLIDE

## Xifrat multiplataforma ##

- Es poden crear _volums_ xifrats amb [Veracrypt](https://www.veracrypt.fr/en/Home.html)
  - dispositius sencers (pendrive)
  - una carpeta (volum virtual)
  - una partició
  - tot el sistema (només windows, equivalent a LUKS)

#VSLIDE

## Xifrat Linux ##

- Es fa durant la instal·lació
  - [LUKS/cryptsetup](https://gitlab.com/cryptsetup/cryptsetup): tot el sistema ([tutorial](https://forums.linuxmint.com/viewtopic.php?t=298852))
  - [eCryptfs](https://www.ecryptfs.org/): només la carpeta personal

#VSLIDE

## Xifrat Mac ##
- Per xifrar el disk mac fa servir un programa anomenay FileVault
- [Documentación oficial de apple](https://support.apple.com/en-us/HT204837)

#VSLIDE

## Xifrat Windows ##
- Bitlocker: xifrat de tot el disc ([Manual oficial](https://support.microsoft.com/en-us/windows/turn-on-device-encryption-0c453637-bc88-5f74-5105-741561aae838))
- [Veracrypt](https://www.veracrypt.fr/en/Home.html): sistema sencer o "volums".

#VSLIDE

## Xifrat Correu electrònic ##
- El emails es xifren només entre dispositiu i servidor
- Google pot llegir els emails dels comptes gmail
- Per xifrar _extrem a extrem_: [GPG](https://gnupg.org/) (claus asimetriques)
- Per fer-lo servir: [Thunderbird](https://support.mozilla.org/es/kb/openpgp-thunderbird-howto-and-faq)
- _amb versions anteriors calia instal·lar l'extensió [Enigmail](https://enigmail.net/)_
